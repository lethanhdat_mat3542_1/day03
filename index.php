<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Programming Day02</title>
    <style>
        body {
            display: flex;
            justify-content: center;
        }

        .wrapper {
            display: inline-block;
            border: 1.5px solid #4475a2;
            padding: 30px 60px;
        }

        .time-box {
            background-color: #f2f2f2;
            width: 350px;
            padding: 10px;
            margin-bottom: 10px;
        }

        label {
            background-color: #5b9bd5;
            width: 150px;
            display: inline-block;
            line-height: 30px;
            padding-left: 10px;
            color: white;
            border: 1.5px solid #4475a2;
        }

        .input-box {
            width: 370px;
            position: relative;
            margin-bottom: 10px;
        }

        .input-radio {
            margin-bottom: 10px;
        }

        .input {
            position: absolute;
            right: 0;
            border: 1.5px solid #4475a2;
            height: 30px;
            width: 180px;
        }

        .button-box {
            margin-top: 30px;
            display: flex;
            justify-content: center;
        }

        .input-username {
            border: 1px solid #4475a2;
            height: 28px;
            width: 180px;
        }

        button {
            background-color: rgb(85, 159, 39);
            ;
            color: white;
            padding: 12px 38px;
            border: 1.5px solid #4475a2;
            border-radius: 7px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <form action="form.php">
            <div class="input-box username-box">
                <label>Họ và tên</label>
                <input type="text" class="input-username" name="hovaten">
            </div>
            <?php
            $arr_khoas = [
                "" => "",
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"
            ];
            $gentle = [
                0 => "Nam",
                1 => "Nữ"
            ]
            ?>
            <div class="input-radio">
                <label>Giới tính</label>
                <?php
                for ($x = 0; $x < count($gentle); $x++) {
                    $sex = $gentle[$x];
                    $arr_key = array_keys($gentle)[$x];
                    echo '<input type="radio" id="gentle" name="gentle" value="'.$arr_key.'" />'.$sex.'';
                }
                ?> 
            </div>
            <div class="input-box password-box">
                <label>Phân Khoa</label>
                <select class="input-username" name="khoa">
                    <?php
                    foreach ($arr_khoas as $key => $arr_khoa) {
                        echo '<option value="' . $key . '">' . $arr_khoa . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="button-box">
                <button>Đăng Ký</button>
            </div>
        </form>
    </div>
</body>

</html>